import cv2
import numpy as np 
 
# read and scale down image
# wget https://bigsnarf.files.wordpress.com/2017/05/hammer.png
img = cv2.imread('images4.jpg')
img_yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)
img_yuv[:,:,0] = cv2.equalizeHist(img_yuv[:,:,0])
img = cv2.cvtColor(img_yuv, cv2.COLOR_YUV2BGR)

lower_red = np.array([0,0,110])
upper_red = np.array([118,118,255])
mask = cv2.inRange(img, lower_red, upper_red)
res = cv2.bitwise_and(img,img, mask=mask)

lower_blue = np.array([68,0,0])
upper_blue = np.array([255,155,155])
mask_blue = cv2.inRange(img, lower_blue, upper_blue)
res_blue = cv2.bitwise_and(img,img, mask=mask_blue)

# gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

res = cv2.bilateralFilter(res, 11, 17, 17)
canny = cv2.Canny(res, 200, 240, 3)

res_blue = cv2.bilateralFilter(res_blue, 11, 17, 17)
canny_blue = cv2.Canny(res_blue, 200, 240, 3)

# threshold image
ret, threshed_img = cv2.threshold(canny, 127, 255, cv2.THRESH_BINARY)
ret_blue, threshed_img_blue = cv2.threshold(canny_blue, 127, 255, cv2.THRESH_BINARY)
# find contours and get the external one
_, contours_red, _ = cv2.findContours(threshed_img, cv2.RETR_TREE,
                cv2.CHAIN_APPROX_SIMPLE)
_, contours_blue, _ = cv2.findContours(threshed_img_blue, cv2.RETR_TREE,
                cv2.CHAIN_APPROX_SIMPLE)

height, width, channels = img.shape
s = height * width * 5 / 100
for c in contours_red:
    # get the bounding rect
    x, y, w, h = cv2.boundingRect(c)
    if w > h * 2 or h > w *2 or w * h < s:
        continue
    # draw a green rectangle to visualize the bounding rect
    cv2.rectangle(img, (x, y), (x+w, y+h), (255, 17, 0), 2)

for c in contours_blue:
    # get the bounding rect
    x, y, w, h = cv2.boundingRect(c)
    if w > h * 2 or h > w *2 or w * h < s:
        continue
    # draw a green rectangle to visualize the bounding rect
    cv2.rectangle(img, (x, y), (x+w, y+h), (7, 7, 255), 2)

cv2.imshow("contours", img)
cv2.waitKey(30000)
cv2.destroyAllWindows()