import os

import cv2
import numpy as np

INPUT_PATH = "/home/daidv/tmp/opencv/test_data/"
OUTPUT_PATH = "/home/daidv/tmp/opencv/output/"


def process_img(input, output):
    "Process img and save it to output"
    img = cv2.imread(input)

    lower_red = np.array([0,0,110])
    upper_red = np.array([118,118,255])

    lower_blue = np.array([68,0,0])
    upper_blue = np.array([255,155,155])

    mask = cv2.inRange(img, lower_red, upper_red)
    res = cv2.bitwise_and(img,img, mask=mask)

    mask_blue = cv2.inRange(img, lower_blue, upper_blue)
    res_blue = cv2.bitwise_and(img,img, mask=mask_blue)

    # gray = cv2.cvtColor(res, cv2.COLOR_BGR2GRAY)
    # gray = cv2.bilateralFilter(gray, 5, 175, 175)
    # gray = cv2.bilateralFilter(gray, 11, 17, 17)
    # ret, thresh = cv2.threshold(gray, 127, 255, 0)
    # img2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    #Canny
    canny = cv2.Canny(res_blue, 80, 240, 3)

    cv2.imwrite(output, img)
    # cv2.namedWindow( "Display frame", cv2.WINDOW_NORMAL)
    # cv2.resizeWindow('Display frame', 600,600)
    cv2.imshow('Display frame',canny)
    cv2.waitKey(50000)
    return canny


def shape_detection(imgray):
    "Detect shapes in imgray"
    _, thresh = cv2.threshold(imgray, 127, 255, 0)
    _, contours, _ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    coordinates = []
    for cnt in contours:
        approx = cv2.approxPolyDP(
            cnt, 0.07 * cv2.arcLength(cnt, True), True)
        if len(approx) == 3:
            coordinates.append([cnt])
            cv2.drawContours(imgray, [cnt], 0, (0, 0, 255), 3)
    cv2.imshow('Display frame', imgray)
    cv2.waitKey(50000)

if __name__ == '__main__':
    shape_detection(process_img('images2.jpg', 'output.jpg'))
    # names = os.listdir(INPUT_PATH)
    # for name in names:
    #     input = INPUT_PATH + name
    #     output = OUTPUT_PATH + name
    #     process_img(input, output)
 