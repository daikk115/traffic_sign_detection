import os

import cv2
import numpy as np


def process_img(input):
    "Process img and save it to output"
    img = cv2.imread(input)

    # Convert BGR to HSV
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    # define range of blue color in HSV
    lower_blue = np.array([110,50,50])
    upper_blue = np.array([130,255,255])

    mask_blue = cv2.inRange(hsv, lower_blue, upper_blue)
    res_blue = cv2.bitwise_and(hsv,hsv, mask=mask_blue)

    canny = cv2.Canny(res_blue, 220, 240, 3)

    cv2.namedWindow( "Display frame", cv2.WINDOW_NORMAL)
    cv2.resizeWindow('Display frame', 600,600)
    cv2.imshow('Display frame',canny)
    cv2.waitKey(50000)

if __name__ == '__main__':
    process_img('images2.jpg')
 