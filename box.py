import cv2
import numpy as np
image_obj = cv2.imread('images.jpg')

gray = cv2.cvtColor(image_obj, cv2.COLOR_BGR2GRAY)
gray = cv2.bilateralFilter(gray, 11, 17, 17)

edged = cv2.Canny(gray,200,240,3)
# edged = cv2.Canny(gray,80,240,3)

kernel_1 = np.ones((2,2), np.uint8)
kernel_2 = np.ones((1,1), np.uint8)

img_dilation = cv2.dilate(edged, kernel_1, iterations=1)
img_erosion = cv2.erode(img_dilation, kernel_2, iterations=1)

 # Crop image
# imCrop = img_erosion[0:100, 0:100]
# Display cropped image
# cv2.imshow("Image Crop", imCrop)

# threshold image
ret, threshed_img = cv2.threshold(img_erosion, 127, 255, cv2.THRESH_BINARY)
# find contours and get the external one
image, contours, hier = cv2.findContours(threshed_img, cv2.RETR_TREE,
                cv2.CHAIN_APPROX_SIMPLE)

for c in contours:
    # get the bounding rect
    x, y, w, h = cv2.boundingRect(c)
    # draw a green rectangle to visualize the bounding rect
    cv2.rectangle(image_obj, (x, y), (x+w, y+h), (7, 7, 255), 2)

cv2.imshow('Erosion', image)
# cv2.imshow('Dilation', img_dilation)
# cv2.imshow('result image', edged)
# cv2.imwrite('output.jpg', img_dilation)
cv2.waitKey(30000)