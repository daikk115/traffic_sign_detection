import os

import cv2
import numpy as np

# INPUT_PATH = "manual_data/"
INPUT_PATH = "../test_data/"
OUTPUT_PATH = "output/"


def draw_traffic_sign(input, ouput):
    img = cv2.imread(input)
    img_yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)
    img_yuv[:,:,0] = cv2.equalizeHist(img_yuv[:,:,0])
    img = cv2.cvtColor(img_yuv, cv2.COLOR_YUV2BGR)

    hsv_blue = cv2.cvtColor(img.copy(), cv2.COLOR_BGR2HSV)
    hsv_red = cv2.cvtColor(img.copy(), cv2.COLOR_BGR2HSV)

    # define range of blue color in HSV
    lower_blue = np.array([110,50,50])
    upper_blue = np.array([130,255,255])
    lower_red = np.array([0,50,50])
    upper_red = np.array([10,255,255])

    mask_blue = cv2.inRange(hsv_blue, lower_blue, upper_blue)
    res_blue = cv2.bitwise_and(hsv_blue,hsv_blue, mask=mask_blue)
    mask_red = cv2.inRange(hsv_red, lower_red, upper_red)
    res_red = cv2.bitwise_and(hsv_red,hsv_red, mask=mask_red)

    res_blue = cv2.bilateralFilter(res_blue, 11, 17, 17)
    res_red = cv2.bilateralFilter(res_red, 11, 17, 17)
    canny_blue = cv2.Canny(res_blue, 220, 240, 3)
    canny_red = cv2.Canny(res_red, 220, 240, 3)

    kernel = np.ones((3, 3), np.uint8)
    kernel_small = np.ones((2, 2), np.uint8)

    # gray = cv2.dilate(canny, kernel, iterations=1)
    # gray = cv2.morphologyEx(gray, cv2.MORPH_OPEN, kernel_small)
    # gray = cv2.erode(gray, kernel, iterations=1)

    # threshold image
    _, threshed_img_red = cv2.threshold(canny_red, 127, 255, cv2.THRESH_BINARY)
    _, threshed_img_blue = cv2.threshold(canny_blue, 127, 255, cv2.THRESH_BINARY)
    # find contours and get the external one
    _, contours_red, _ = cv2.findContours(threshed_img_red, cv2.RETR_TREE,
                    cv2.CHAIN_APPROX_SIMPLE)
    _, contours_blue, _ = cv2.findContours(threshed_img_blue, cv2.RETR_TREE,
                    cv2.CHAIN_APPROX_SIMPLE)

    height, width, channels = img.shape
    s = height * width * 5 / 100
    print "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
    print height, width, input
    for c in contours_red:
        # get the bounding rect
        x, y, w, h = cv2.boundingRect(c)
        # if w * h < s:
        #     print w, h, s
        #     continue
        # draw a green rectangle to visualize the bounding rect
        print w, h, s
        cv2.rectangle(img, (x, y), (x+w, y+h), (255, 17, 0), 2)

    for c in contours_blue:
        # get the bounding rect
        x, y, w, h = cv2.boundingRect(c)
        # if w * h < s:
            # print w, h, s
            # continue
        # draw a green rectangle to visualize the bounding rect
        print w, h, s
        cv2.rectangle(img, (x, y), (x+w, y+h), (7, 7, 255), 2)

    # cv2.imshow("contours", canny_blue)
    # cv2.waitKey(30000)
    cv2.imwrite(output, img)


if __name__ == '__main__':
    names = os.listdir(INPUT_PATH)
    # names = ['imag.jpg']
    for name in names:
        input = INPUT_PATH + name
        output = OUTPUT_PATH + name
        draw_traffic_sign(input, output)
