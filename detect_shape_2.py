import math
import numpy as np
import cv2

img = cv2.imread('images8.jpg')

#grayscale
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

ret, thresh = cv2.threshold(gray, 127, 255, 0)
img2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

#Canny
canny = cv2.Canny(img2,80,240,3)

cv2.imshow('canny',canny)
cv2.waitKey(10000)
